# Mesopotamian

[![Build Status](https://drone.io/bitbucket.org/janlindblom/babylon/status.png)](https://drone.io/bitbucket.org/janlindblom/babylon/latest)
[![Gem](https://img.shields.io/gem/v/mesopotamian.svg?style=flat-square)](https://rubygems.org/gems/mesopotamian)
[![Documentation](http://img.shields.io/badge/docs-rdoc.info-blue.svg?style=flat-square)](http://www.rubydoc.info/gems/mesopotamian/frames)
[![License](http://img.shields.io/badge/license-MIT-yellowgreen.svg?style=flat-square)](#license)

Mesopotamian mathematics for Ruby. Version 𒁹.

Adds support for Babylonian numerals and conversions between Mesopotamian sexagesimal numbers and regular decimal integers. All the named numerals are available: 𒁹, 𒈫, 𒐈, 𒐉, 𒐊, 𒐋, 𒐌, 𒐍, 𒐎, 𒌋, 𒌋𒌋, 𒌍, 𒄭 and 𒐐.

Adds a new type: Mesopotamian. This represents sexagesimal babylonian numerals in single digit or array form as well as cuneiform string format.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'mesopotamian'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install mesopotamian

## Usage

Use Mesopotamian objects to represent numeral in Mesopotamian base 60 (sexagecimals):

```ruby
num = Mesopotamian(65535)
num.to_s
=> "𒌋𒐍  𒌋𒈫  𒌋𒐊"
num.to_i
=> 65535
num.to_a
=> [18, 12, 15]
```

# Background

Most if not all of the actuals have been collected from http://it.stlawu.edu/~dmelvill/mesomath/index.html

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://bitbucket.org/janlindblom/babylon/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request