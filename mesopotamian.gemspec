# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mesopotamian_math/numerals'
require 'mesopotamian_math/conversions'
require 'mesopotamian_math/version'

Gem::Specification.new do |spec|
  spec.name          = "mesopotamian"
  spec.version       = MesopotamianMath::Conversions.dec_value(MesopotamianMath::VERSION).to_s
  spec.authors       = ["Jan Lindblom"]
  spec.email         = ["janlindblom@fastmail.fm"]

  spec.summary       = %q{Mesopotamian mathematics.}
  spec.homepage      = "https://bitbucket.org/janlindblom/babylon"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "pry", "~> 0.10.2"
  spec.add_development_dependency "yard", "~> 0.8.7.6"
  spec.add_development_dependency "rspec", "~> 3.3.0"
end
