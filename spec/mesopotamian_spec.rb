require 'spec_helper'

describe Mesopotamian do
  it "can create new Mesopotamian numerals through extensions on Integer and Array" do
    m1 = 65535.to_m
    m2 = [23,14].to_m
    expect(m1).to be_a Mesopotamian
    expect(m1.to_i).to eq 65535
    expect(m2).to be_a Mesopotamian
    expect(m2.to_a).to eq [23,14]
  end
  
  it "can create new Mesopotamian numerals by calling Mesopotamian(n) and Mesopotamian.new(n)" do
    m1 = Mesopotamian(65535)
    m2 = Mesopotamian([23,14])
    m3 = Mesopotamian.new(65535)
    m4 = Mesopotamian.new([23,14])
    expect(m1).to be_a Mesopotamian
    expect(m1.to_i).to eq 65535
    expect(m2).to be_a Mesopotamian
    expect(m2.to_a).to eq [23,14]
    expect(m3).to be_a Mesopotamian
    expect(m3.to_i).to eq 65535
    expect(m4).to be_a Mesopotamian
    expect(m4.to_a).to eq [23,14]
  end

  context("given a Mesopotamian object") do
    before(:context) do
      @m = 65535.to_m
    end
    it "can return array notation format" do
      expect(@m.to_a).to be_a Array
      expect(@m.to_a).to eq [18, 12, 15]
    end

    it "can return string notation format" do
      expect(@m.to_s).to be_a String
      expect(@m.to_s).to eq "𒌋𒐍  𒌋𒈫  𒌋𒐊"
    end

    it "can return the base 10 integer for this object" do
      expect(@m.to_i).to be_a Integer
      expect(@m.to_i).to eq 65535
    end
  end

  context("basic mathematics") do
    before(:context) do
      @a = 1337.to_m
      @b = 101.to_m
    end
    it "handles addition" do
      expect(@a + @b).to eq 1438.to_m
    end
    it "handles multiplication" do
      expect(@a * @b).to eq 135037.to_m
    end
    it "handles (integer) division" do
      expect(@a / @b).to eq 13.to_m
    end
    it "can raise to a power" do
      expect(@b ** 2.to_m).to eq 10201.to_m
    end
  end
  context("boolean arithmetics") do
    before(:context) do
      @a = 1337.to_m
      @b = 4.to_m
    end
    it "can shift left and right" do
      expect(@a << @b).to eq 21392.to_m
      expect(@a >> @b).to eq 83.to_m
    end
    it "does and + or" do
      expect((@a & @b).to_i).to eq 0
      expect(@a | @b).to eq 1341.to_m
    end
  end
end
