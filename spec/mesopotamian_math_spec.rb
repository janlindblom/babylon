require 'spec_helper'

describe MesopotamianMath do
  it 'has a version number' do
    expect(MesopotamianMath::VERSION).not_to be nil
  end

  describe MesopotamianMath::Numerals do
    it 'has all the named numerals in place' do
    expect(MesopotamianMath::Numerals::DISH).to be_a String
    expect(MesopotamianMath::Numerals::MIN).to be_a String
    expect(MesopotamianMath::Numerals::ESH).to be_a String
    expect(MesopotamianMath::Numerals::LIMMU).to be_a String
    expect(MesopotamianMath::Numerals::IA).to be_a String
    expect(MesopotamianMath::Numerals::ASH).to be_a String
    expect(MesopotamianMath::Numerals::IMIN).to be_a String
    expect(MesopotamianMath::Numerals::USSU).to be_a String
    expect(MesopotamianMath::Numerals::ILIMMU).to be_a String
    expect(MesopotamianMath::Numerals::U).to be_a String
    expect(MesopotamianMath::Numerals::UDISH).to be_a String
    expect(MesopotamianMath::Numerals::MAN).to be_a String
    expect(MesopotamianMath::Numerals::USHU).to be_a String
    expect(MesopotamianMath::Numerals::HI).to be_a String
    expect(MesopotamianMath::Numerals::NINNU).to be_a String
    expect(MesopotamianMath::Numerals::GISH).to be_a String
    expect(MesopotamianMath::Numerals::DISHU).to be_a String
    expect(MesopotamianMath::Numerals::GESU).to be_a String
    expect(MesopotamianMath::Numerals::LIM).to be_a String
    expect(MesopotamianMath::Numerals::SHAR).to be_a String
    end
  end
end
