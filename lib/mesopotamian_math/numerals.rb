module MesopotamianMath
  # Named regular numerals as strings
  module Numerals
    # diš
    DISH    = "𒁹"
    # min
    MIN     = "𒈫"
    # eš
    ESH     = "𒐈"
    # limmu
    LIMMU   = "𒐉"
    # ia
    IA      = "𒐊"
    # aš
    ASH     = "𒐋"
    # imin
    IMIN    = "𒐌"
    # ussu
    USSU    = "𒐍"
    # ilimmu
    ILIMMU  = "𒐎"
    # u
    U       = "𒌋"
    # u.dish
    UDISH   = "𒌋𒁹"
    # man
    MAN     = "𒌋𒌋"
    # ušu
    USHU    = "𒌍"
    # hi
    HI      = "𒄭"
    # ninnu
    NINNU   = "𒐐"
    # giš
    GISH    = "𒁹"
    # diš.u
    DISHU   = "𒁹𒌋"
    # gesu
    GESU    = "𒌋   "
    # lim
    LIM     = "𒌋𒐋  𒄭 "
    # šar
    SHAR    = "𒁹      "
  end
end