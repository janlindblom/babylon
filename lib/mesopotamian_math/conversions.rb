require "mesopotamian_math/numerals"

module MesopotamianMath
  # Conversions between decimal (base 10) and sexagesimal (base 60).
  module Conversions
    include Numerals

    # Converts an integer into sexagecimal, array syntax.
    # @param [Integer] decimal
    # @return [Integer,Array]
    def self.sexa_value(decimal)
      raise ArgumentError.new "The argument #{decimal} is not integer" unless decimal.is_a?(Integer)
      index = 0
      input = decimal
      result = []
      while input != 0
        remainder = input % 60
        result.unshift remainder
        input = input / 60
      end
      result
    end

    # Convert a sexagesimal number, in integer or array form, into a string
    # with babylonian cuneiform numerals.
    # @param [Array,Integer] numeral
    # @return [String]
    def self.sexa_to_string(numeral)
      raise ArgumentError.new "Argument #{numeral} is neither Integer nor Array" unless (numeral.is_a?(Integer) || numeral.is_a?(Array))
      n = [numeral] if numeral.is_a? Integer
      n = numeral if numeral.is_a? Array
      result = []
      n.each do |digit|
        result << babylonian_digit(digit)
      end
      result.join "  "
    end

    # Translates _simple_ mesopotamian numerals into integers. Only the base set
    # of named numerals are, see {Babylon::Numerals}.
    # @param [String] babylonian
    # @return [Integer]
    def self.dec_value(babylonian)
      case babylonian
        when DISH   then 1
        when MIN    then 2
        when ESH    then 3
        when LIMMU  then 4
        when IA     then 5
        when ASH    then 6
        when IMIN   then 7
        when USSU   then 8
        when ILIMMU then 9
        when U      then 10
        when UDISH  then 11
        when MAN    then 20
        when USHU   then 30
        when HI     then 40
        when NINNU  then 50
        when GISH   then 60
        when DISHU  then 70
        when GESU   then 600
        when LIM    then 1000
        when SHAR   then 3600
      end
    end

    private

    def self.babylonian_digit(digit)
      raise ArgumentError.new "Argument #{digit} is not an Integer" unless digit.is_a? Integer
      case digit
        when 0 then " "
        when 1 then DISH
        when 2 then MIN
        when 3 then ESH
        when 4 then LIMMU
        when 5 then IA
        when 6 then ASH
        when 7 then IMIN
        when 8 then USSU
        when 9 then ILIMMU
        when 10 then U
        when 11 then UDISH
        when 20 then MAN
        when 30 then USHU
        when 40 then HI
        when 50 then NINNU
        when 60 then GISH
        when 70 then DISHU
        when 600 then GESU
        when 1000 then LIM
        when 3600 then SHAR
        else
          digits = digit.to_s.chars.map{ |x| x.to_i }
          place = digits.length - 1
          digits.each_with_index do |d,i|
            digits[i] = d * 10 ** place
            place -= 1
          end
          digits.map{ |x| babylonian_digit x }.join ""
      end
    end

  end
end