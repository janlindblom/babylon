require "mesopotamian_math/version"
require "mesopotamian_math/numerals"
require "mesopotamian_math/conversions"

# Mesopotamian numbers
class Mesopotamian < Numeric
  include Comparable
  
  # Create a new mesopotamian numeral
  # @param [Numeric,Array] num
  def initialize(num)
    case num
    when Numeric
      @i = num.to_i
    when Array
      sum = 0
      num.reverse.each_with_index do |n,i|
        sum += n * 60**i
      end
      @i = sum
    else
      raise ArgumentError.new "Cannot convert #{num}"
    end
  end

  # Return a string representation of this Mesopotamian.
  # @return [String]
  def to_s
    @s ||= build_string.freeze
  end

  # Returns an integer representation of this Mesopotamian in base 10.
  # @return [Integer]
  def to_i
    @i
  end

  alias to_int to_i

  def to_a
    MesopotamianMath::Conversions.sexa_value @i
  end

  # As this is already a Mesopotamian, simply return itself.
  # @return [Mesopotamian]
  def to_m
    self
  end

  alias to_sex to_m

  # Returns +true+ if +num+ and this are the same type and have equal values.
  # @return [Boolean]
  def eql?(num)
    self.class.equal?(num.class) && @i == num.to_i
  end

  alias == eql?

  # @return [Numeric]
  def hash
    @i.to_hash
  end

  # @return [Boolean]
  def zero?
    @i.zero?
  end

  # @return [Boolean]
  def nonzero?
    @i.nonzero?
  end

  # Returns +true+ if this is an odd number.
  # @return [Boolean]
  def odd?
    @i.odd?
  end

  # Returns +true+ if this is an even number.
  # @return [Boolean]
  def even?
    @i.even?
  end

  # Returns +false+ if +a+ is less than zero or if +b+ is greater than zero, true otherwise.
  # @return [Boolean]
  def between?(a, b)
    @i.between? a, b
  end

  # Since a Mesopotamian is an Integer, this always returns +true+.
  # @return [Boolean]
  def integer?
    true
  end

  # If this is the same type as +o+, returns an array containing this and +num+.
  # @return [Array<Mesopotamian>]
  def coerce(o)
    [Mesopotamian.new(o.to_int), self]
  end

  # Unary Plus—Returns the receiver's value.
  # @return [Mesopotamian]
  def +@
    self
  end

  # Unary Minus—Returns the receiver's value, negated.
  # @return [Mesopotamian]
  def -@
    Mesopotamian.new(-@i)
  end

  # @return [Mesopotamian]
  def +(o)
    op(:+, o)
  end

  # @return [Mesopotamian]
  def -(o)
    op(:-, o)
  end

  # @return [Mesopotamian]
  def *(o)
    op(:*, o)
  end

  # @return [Mesopotamian]
  def /(o)
    op(:/, o)
  end

  # @return [Mesopotamian]
  def **(o)
    op(:**, o)
  end

  # @return [Mesopotamian]
  def <<(o)
    Mesopotamian.new(@i << o.to_int)
  end

  # @return [Mesopotamian]
  def >>(o)
    Mesopotamian.new(@i >> o.to_int)
  end

  # @return [Mesopotamian]
  def &(o)
    Mesopotamian.new(@i & o.to_int)
  end

  # @return [Mesopotamian]
  def |(o)
    Mesopotamian.new(@i | o.to_int)
  end

  # Returns zero if this equals +other+, otherwise +nil+ is returned if the
  # two values are incomparable.
  # @return [Numeric]
  def <=>(o)
    case o
    when Mesopotamian
      @i <=> o.to_i
    when Numeric
      @i <=> o
    else
      a, b = o.coerce(self)
      a <=> b
    end rescue nil
  end

  # @return [Mesopotamian]
  def freeze
    to_m
    super
  end

  private

  def build_string
    sexa = MesopotamianMath::Conversions.sexa_value @i
    if (sexa.is_a?(Integer) || sexa.is_a?(Array))
      return MesopotamianMath::Conversions.sexa_to_string sexa
    else
      # We shouldn't be able to end up here
      raise TypeError.new "Something is wrong with the value #{sexa}, it's neither Integer nor Array"
    end
    ""
  end

  def op(sym, o)
    case o
    when Mesopotamian
      Mesopotamian.new(@i.send(sym, o.to_i))
    when Numeric
      Mesopotamian.new(@i.send(sym, o))
    else
      a, b = o.coerce(self)
      a.send(sym, b)
    end
  end

end

# # Create a new mesopotamian numeral
def Mesopotamian(num)
  Mesopotamian.new(num)
end

# Extensions to the Integer class
class Integer
  # Convert this Integer to a Mesopotamian
  def to_m
    Mesopotamian.new(to_i)
  end
  alias to_sex to_m
end

# Extensions to the Array class
class Array
  # Convert this Array to a Mesopotamian
  def to_m
    Mesopotamian.new(to_a)
  end
  alias to_sex to_m
end